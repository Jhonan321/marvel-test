import React from 'react'
import Image from 'next/image'
import Link from 'next/link'
import Logo from '../../public/marvel.svg'
import Search from './Search'

const Header = () => {
    return (
        <header className='bg-black container text-white flex flex-row justify-between min-w-full content-center items-center p-4'>
            <div className="page-logo cursor-pointer">
                <Link href='/' passHref>
                    <Image src={Logo} alt='Marvel Logo' width={120} height={50} />
                </Link>
            </div>
            {/*<Search />*/}
        </header>
    )
}

export default Header