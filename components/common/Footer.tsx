import React from 'react'
import styles from '../../styles/Home.module.css'
import Logo from '../../public/marvel.svg'
import Image from 'next/image'

const Footer = () => {
    return (
        <footer className={styles.footer}>
            <Image src={Logo} alt='Marvel Logo' width={100} height={25}/>
            <span>
                ©2022 MARVEL
            </span>
        </footer>
    )
}

export default Footer