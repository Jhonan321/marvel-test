import axios from "axios";
import react, { ChangeEvent, FC, useState, useEffect } from "react";

interface IData {
    name: string;
    code: string;
}

const Search = () => {
    const [search, setSearch] = useState({
        text: "",
        suggestions: []
    });
    const [isComponentVisible, setIsComponentVisible] = useState(true);

    const hash = '&hash=5e733ff35c80675e242c956014dea05d'
    const itemUrl= `https://gateway.marvel.com:443/v1/public/comics/${search.text ? `?title=${search.text}&orderBy=title` : ''}&ts=1&apikey=51ee321cc51362ef7106ba01bc84de1a${hash}`
    const getComicsByTitle = async () => {
        const res : any = await axios.get(itemUrl)
        console.log("🚀 ~ file: Search.tsx ~ line 24 ~ data ~ res", res)

        return res.data.data.results
    }
    const data : any = []
    useEffect(() => {
        getComicsByTitle()
    }, [search.text])

    const onTextChanged = (e: ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        let suggestions : any = [];
        if (value.length > 0) {
            const regex = new RegExp(`^${value}`, "i");
            //suggestions = data.sort().filter((v: IData) => regex.test(v.title));
        }
        setIsComponentVisible(true);
        setSearch({ suggestions, text: value });
    };

    const suggestionSelected = (value: IData) => {
        setIsComponentVisible(false);

        setSearch({
            text: value.name,
            suggestions: []
        });
    };

    const { suggestions } = search;

    return (
        <div className="w-1/5">
            <div
                onClick={() => setIsComponentVisible(false)}
                className={`${isComponentVisible ? 'block' : 'hidden'}`}
                style={{
                    width: "200vw",
                    height: "200vh",
                    backgroundColor: "transparent",
                    position: "fixed",
                    zIndex: 0,
                    top: 0,
                    left: 0
                }}
            />
            <input
                className='p-2 w-full text-black max-w-full'
                type="text"
                value={search.text}
                onChange={onTextChanged}
                placeholder='Search comic by title'
            />
            {suggestions.length > 0 && isComponentVisible && (
                <div>
                    { suggestions.map((item: IData) => (
                        <div key={item.code}>
                            <div
                                key={item.code}
                                onClick={() => suggestionSelected(item)}
                            >
                                {item.name}
                            </div>
                        </div>
                    ))}
                </div>
            )}
        </div>
    )
}

export default Search