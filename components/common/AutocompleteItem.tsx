import React from "react"
import Link from "next/link"
import Image from "next/image"

interface Props {
    id: number,
    title: string,
    img: string,
    price: number
}

const AutocompleteItem = ({ id, title, img, price } : Props) => {
    return (
      <li>
        <Link href={`/detail/${id}`}>
          <a className='hover:bg-blue-300 flex gap-4 p-4'>
            <Image src={img} alt={title} className='object-contain' width={20} height={12} />
            <div>
              <h3 className='text-sm font-semibold'>{title}</h3>
              <p className='text-xs text-gray-600'>{price}</p>
            </div>
          </a>
        </Link>
      </li>
    )
  }

export default AutocompleteItem