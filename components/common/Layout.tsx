import React from 'react'
import Header from './Header'
import Head from 'next/head'
import styles from '../../styles/Home.module.css'
import Footer from './Footer'

interface Props {
  children?: React.ReactNode
}

const Layout = ({ children }: Props) => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Marvel Test</title>
        <meta name="description" content="Marvel Test" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <main className={styles.main}>
        { children }
      </main>
      <Footer />
    </div>
  )
}

  export default Layout