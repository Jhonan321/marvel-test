import React, { useState, useEffect } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import { useDispatch, useSelector } from 'react-redux'
import { addToFavorites, removeFromFavorites } from '../redux/actions'
import { RootState } from 'redux/reducers'

interface Props {
    comic : any,
    isFavorite?: boolean
}

const ComicItem = ({comic , isFavorite = false}: Props) => {
    const dispatch = useDispatch()

    const { favorites } = useSelector((state : RootState) => state.comicsList)
    const { id, title, images } = comic
    const finalImg : string = comic && images?.length === 0 ? '/no-img.png' : `${images[0]?.path}.${images[0]?.extension}`

    const addFavorites = (e: any) => {
        e.stopPropagation()
        dispatch(addToFavorites(comic))
    }
    const removeFavorites = (e: any) => {
        e.stopPropagation()
        dispatch(removeFromFavorites(comic))
    }

    const validateIfFavorite = () => favorites.some((fav: any) => fav.id === id)

    return (
        <li className={`bg-white p-6 m-8 text-center cursor-pointer border shadow-xl transition hover:scale-[1.03]`} >
            <Link href={`/ComicDetail/${id}`} passHref>
                <div>
                    <div className='max-w-100 relative'>
                        <Image
                            loader={() => finalImg}
                            src={finalImg}
                            width="100%" height="100%" layout="responsive" objectFit="contain"
                            unoptimized
                            alt='Comic Img'
                        />
                    </div>
                    <h2 className=' mt-3 text-base font-semibold'>
                        { title }
                    </h2>
                    {!isFavorite ? (<button
                        type='button'
                        className='bg-red-500 px-4 py-2 mt-4 text-white shadow-md transition border border-red-500 hover:bg-white hover:text-red-500 disabled:bg-gray-600 disabled:border-gray-600 disabled:hover:border-gray-600 disabled:hover:text-white'
                        onClick={(e) => addFavorites(e)}
                        disabled={validateIfFavorite()}
                    >
                        Add to Favorites
                    </button>)
                    : (<button
                        type='button'
                        className='bg-red-500 px-4 py-2 mt-4 text-white shadow-md transition border border-red-500 hover:bg-white hover:text-red-500'
                        onClick={(e) => removeFavorites(e)}
                    >
                        Remove Favorites
                    </button>)}
                </div>
            </Link>
        </li>
    )
}

export default ComicItem