import axios from 'axios'
import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import ComicItem from './ComicItem'
import { useDispatch } from 'react-redux'
import { setComics, addNextPage } from '../redux/actions'
import { RootState } from '../redux/reducers'
import InfiniteScroll from 'react-infinite-scroll-component'

const ComicsList = () => {
    const { comics, favorites } = useSelector((state : RootState) => state.comicsList)
    const dispatch = useDispatch()

    const ts = '?ts=1'
    const orderBy = '&orderBy=title'
    const limit = '&limit=21'
    const hash = '&hash=5e733ff35c80675e242c956014dea05d'
    const url = 'https://gateway.marvel.com:443/v1/public/comics'
    const offset = comics.length <= 21 ? '' : `?offset=${comics.length}`

    const finalUrl = `${url}${ts}${orderBy}${limit}&apikey=51ee321cc51362ef7106ba01bc84de1a${hash}`

    const getComics = async () => {
        const res : any = await axios.get(finalUrl).catch(err => { console.log('ERROR', err) })
        dispatch(setComics(res.data.data.results))
    }

    const getNextPage = async() => {
        const res = await axios.get(`${url}${ts}${orderBy}${limit}${offset}&apikey=51ee321cc51362ef7106ba01bc84de1a${hash}`)
        dispatch(addNextPage(res.data.data.results))
    }

    useEffect(() => {
        getComics()
    }, [])

    const showFavorites = () => {
        const res = favorites.map((fav: any, i: number) => (
            <ComicItem key={i} comic={fav} isFavorite={true} />
        ))

        return res
    }

    useEffect(() => {
        showFavorites()
    }, [favorites])

    return (
        <div>
            {favorites.length !== 0 &&
                <div className="favorites-list mx-8">
                    <h1 className='text-4xl text-red-500 mx-8 font-semibold'>Favorites List</h1>
                    <ul className='container grid grid-cols-3'>
                        { showFavorites() }
                    </ul>
                </div>
            }
            <div className="comics-list mx-8">
                <h1 className='text-4xl text-red-500 mx-8 font-semibold'> Comics List</h1>
                <ul className='container'>
                    <InfiniteScroll
                        className='w-full grid grid-cols-3'
                        dataLength={700} //This is important field to render the next data
                        next={getNextPage}
                        hasMore={true}
                        loader={<h4>Loading...</h4>}
                        endMessage={
                            <p className='text-center'>
                                <b>Yay! You have seen it all</b>
                            </p>
                        }
                    >
                    {
                        comics?.map((comic: any, i: number) => (
                            <ComicItem key={i} comic={comic} />
                            ))
                        }
                    </InfiniteScroll>
                </ul>
            </div>
        </div>
    )
}

export default ComicsList
