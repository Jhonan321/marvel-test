import React, { ReactElement, useEffect } from 'react'
import { useRouter } from 'next/router'
import Image from 'next/image'
import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'
import { setSelectedComic } from '../../redux/actions'
import { RootState } from '../../redux/reducers'
import Layout from 'components/common/Layout'

const ComicDetail = (): ReactElement => {
    const { query: { id } } = useRouter()
    const router = useRouter()

    const dispatch = useDispatch()
    const comicDetails = useSelector((state : RootState) => state.comicsList.selectedComic)

    const { title, description, images, urls } = comicDetails
    //const finalImg : string = comicDetails && images?.length === 0 ? '/no-img.png' : `${images[0].path}.${images[0].extension}`

    const hash = '&hash=5e733ff35c80675e242c956014dea05d'
    const itemUrl= `https://gateway.marvel.com:443/v1/public/comics/${id}?ts=1&apikey=51ee321cc51362ef7106ba01bc84de1a${hash}`

    const getComicById = async () => {
        const res : any = await axios.get(itemUrl).catch(err => { console.log('ERROR', err) })

        dispatch(setSelectedComic(res.data.data.results[0]))
    }

    useEffect(() => {
        console.log("🚀 ~ file: [id].tsx ~ line 18 ~ ComicDetail ~ comicDetails", images)
        console.log("🚀 ~ file: [id].tsx ~ line 18 ~ ComicDetail ~ comicDetails", comicDetails)
    getComicById()
    }, [])


    return (
        <Layout>
            <span
                className='cursor-pointer'
                onClick={() => router.back()}
            >
                &#x2b05; Go Back
            </span>
            <div>
                <h1 className='text-4xl text-red-500 mx-8 font-semibold'>
                    { title }
                </h1>
            </div>
            <div>
                <p>
                    {description && description}
                </p>
            </div>
        </Layout>
    )
}

export default ComicDetail