
import type { NextPage } from 'next'
import styles from '../styles/Home.module.css'
import ComicsList from '../components/ComicsList'
import Layout from '../components/common/Layout'

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <Layout>
        <ComicsList />
      </Layout>
    </div>
  )
}

export default Home
