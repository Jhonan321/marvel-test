import { ActionTypes } from "../../actions/action-types";

const initialState = {
    comics : [],
    selectedComic: {},
    favorites: []
}

export const comicsReducer = (state: any = initialState, action : any) => {
    switch (action.type) {
        case ActionTypes.SET_COMICS:
            return {
                ...state,
                comics: action.payload
            }

        case ActionTypes.SET_SELECTED_COMIC:
            return {
                ...state,
                selectedComic: action.payload
            }

        case ActionTypes.ADD_TO_FAVORITES:
            return {
                ...state,
                favorites: [...state.favorites, action.payload]
            }

        case ActionTypes.REMOVE_FROM_FAVORITES:
            return {
                ...state,
                favorites :  action.payload
            }

        case ActionTypes.ADD_NEXT_PAGE:
            return {
                ...state,
                comics: action.payload
            }

        default:
            return state
    }
}