import { combineReducers } from "redux";
import { comicsReducer } from "./comics";

const reducers = combineReducers({
    comicsList: comicsReducer,
})

export type RootState = ReturnType<typeof reducers>
export default reducers