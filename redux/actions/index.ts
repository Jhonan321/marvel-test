import { ActionTypes } from "./action-types"
import store from "redux/store"



export const setComics = (comics: any) => {
    return {
        type: ActionTypes.SET_COMICS,
        payload: comics,
    }
}

export const setSelectedComic = (comic: any) => {
    return {
        type: ActionTypes.SET_SELECTED_COMIC,
        payload: comic,
    }
}

export const addToFavorites = (comic: any) => {
    return {
        type: ActionTypes.ADD_TO_FAVORITES,
        payload: comic,
    }
}

export const removeFromFavorites = (comic: any) => {
    const state = store.getState().comicsList
    const newState = state.favorites.filter((fav: any) => {
        return fav.id !== comic.id
    })
    return {
        type: ActionTypes.REMOVE_FROM_FAVORITES,
        payload: newState,
    }
}

export const addNextPage = (comics: any) => {
    const state = store.getState().comicsList.comics
    const newState = state.concat(comics)
    return {
        type: ActionTypes.ADD_NEXT_PAGE,
        payload: newState,
    }
}