import axios from "axios"
import { useDispatch } from 'react-redux'
import { setComics } from '../actions'

const getComics = () => async () => {
    const dispatch = useDispatch()
    const ts = '?ts=1'
    const orderBy = '&orderBy=title'
    const limit = '&limit=21'
    const hash = '&hash=5e733ff35c80675e242c956014dea05d'
    const url = 'https://gateway.marvel.com:443/v1/public/comics'

    const finalUrl = `${url}${ts}${orderBy}${limit}&apikey=51ee321cc51362ef7106ba01bc84de1a${hash}`

    const res : any = await axios.get(finalUrl).catch(err => { console.log('ERROR', err) })
    dispatch(setComics(res.data.data.results))
}

export default getComics